Estratégias em um Novo Paradigma Globalizado
 Percebemos, cada vez mais, que a revolução dos costumes pode nos levar a considerar a reestruturação das regras de conduta normativas. 
 Nunca é demais lembrar o peso e o significado destes problemas, uma vez que a complexidade dos estudos efetuados exige a precisão e 
 a definição dos métodos utilizados na avaliação de resultados.
